#include <iostream>
using namespace std;
#include "Position.hpp"


class Dame {
  bool estBlanche;

  public:
    Dame(bool estBlanche) : estBlanche(estBlanche) {
      cout << "Dame()"  << endl;
    }

    ~Dame() {
      cout << "~Dame()"  << endl;
    }
    
    bool getEstBlanche() {
      return estBlanche;
    }
};

class Case {
  Dame* dame;

  public:
    Case () {
      dame = nullptr;
    }

    ~Case() {
      if (!estVide()) {
        delete this->dame;
        this->dame = nullptr;
      }
    }

    void setDame(Dame * dame) {
      this->dame = dame;
    }

    Dame* retireDame() {
      Dame* tmp = this->dame;
      this->dame = nullptr;
      return tmp;
    }

    bool estVide() {
      return this->dame == nullptr;
    }

    string toString() {
      if (this->dame == nullptr)
        return "_";
      // else
      // a->b == (*a).b
      if (this->dame->getEstBlanche()) 
        return "B";
      return "N";
    }

    bool aDameDeCouleur(bool estBlanche) {
      if (this->dame == nullptr)
        return false;
      return this->dame->getEstBlanche() == estBlanche;

    }
};

class Plateau {

  const static int NB_LIGNES = 8;
  const static int NB_COLONNES = 8;

  Case cases[NB_LIGNES][NB_COLONNES];

  void afficheNumeroColonnes() {
    cout << "  ";
    for (int i = 0; i < NB_COLONNES; i++)
    {
      cout << i << " ";
    }
    cout << endl;
  }

  void afficheLigne(int ligne) {
    cout << ligne << " ";
    for (int i = 0; i < NB_COLONNES; i++)
    {
      cout << cases[ligne][i].toString() << " ";
    }
    cout << endl;
  }

  public:
    Plateau() {
      for(int i =0; i < NB_COLONNES; i+=2) {
        cases[0][i].setDame(new Dame(false));
        cases[1][i+1].setDame(new Dame(false));
        cases[2][i].setDame(new Dame(false));
        cases[5][i+1].setDame(new Dame(true));
        cases[6][i].setDame(new Dame(true));
        cases[7][i+1].setDame(new Dame(true));
      }
    }

    Case& getCase(Position pos) {
      int ligne = pos.getLigne();
      int colonne = pos.getColonne();
      return cases[ligne][colonne];
    }

    bool aDameDeCouleurSur(bool estBlanche, Position position) {
      return getCase(position).aDameDeCouleur(estBlanche);
    }

    bool aCaseVide(Position position) {
      return getCase(position).estVide();
    }

    void affiche() {
      afficheNumeroColonnes();
      for(int i = 0; i < NB_LIGNES; i++) {
        afficheLigne(i);
      }
    }

    void deplace(Position initiale, Position finale) {
      Case& caseInitiale = getCase(initiale);
      Case& caseFinale = getCase(finale);
      Dame* dame = caseInitiale.retireDame();
      caseFinale.setDame(dame);
    }

};

class Jeu {
  Plateau plateau;
  bool tourBlancs;

  bool peutDeplacerDame(Position initiale, Position finale) {
    if(this->plateau.aDameDeCouleurSur(tourBlancs, initiale)) {
      if(this->plateau.aCaseVide(finale)) {
        Position delta = finale.difference(initiale);
        // cout << delta.getLigne() << "," << delta.getColonne();
        if ((tourBlancs && delta.getLigne() == -1) || 
            (!tourBlancs && delta.getLigne() == 1)) {
              if (abs(delta.getColonne()) == 1) {
                return true;
              }
            }
      }
    }
    return false;
  }

  public:
    Jeu () {
      tourBlancs = true;
    }
    void affiche() {
      plateau.affiche();
      if (tourBlancs) {
        cout << "Au tour des blancs !" << endl;
      } else {
        cout << "Au tour des noirs !" << endl;
      }
    }
    
    void joue(Position initiale, Position finale){
      if(peutDeplacerDame(initiale, finale)) {
        plateau.deplace(initiale, finale);
        changeTour();
      }
    }

    void changeTour() {
      tourBlancs = !tourBlancs;
    }
};

int main(int argc, char const *argv[]) {
  Jeu jeu;
  jeu.affiche();
  jeu.joue(Position(5,1),Position(4,2));
  jeu.affiche();
  jeu.joue(Position(2,0),Position(3,1));
  jeu.affiche();

  return 0;
}