
class Position {
  int ligne,colonne;
  public:
    Position(int ligne,int colonne);
    int getLigne();
    int getColonne();
    Position difference(Position position);
};