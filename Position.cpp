#include "Position.hpp"

Position::Position(int ligne,int colonne) : ligne(ligne),colonne(colonne) {}

int Position::getLigne() {
  return ligne;
}
int Position::getColonne() {
  return colonne;
}

Position Position::difference(Position position) {
  return Position(ligne - position.ligne, colonne - position.colonne);
}
